﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace PowerAssert.NUnitMapping
{
    public static class NUnitAssertions
    {
        private static Dictionary<Method, Action<object, object>> _mappings = new Dictionary<Method, Action<object, object>>
        {
            {new Method(typeof(Object), "Equals"), Assert.AreEqual},
            {new Method(typeof(string), "Contains"), (left, right) => StringAssert.Contains((string) left, (string) right)}                                                               ,
        };

        public static Dictionary<Method, Action<object, object>> Mappings
        {
            get { return _mappings; }
        }
    }

    public class Method
    {
        public Type TypeName { get; set; }
        public String MethodName { get; private set; }

        public Method(Type type, string methodName)
        {
            TypeName = type;
            MethodName = methodName;
        }

        protected bool Equals(Method other)
        {
            return string.Equals(TypeName, other.TypeName) && string.Equals(MethodName, other.MethodName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Method)) return false;
            return Equals((Method)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((TypeName != null ? TypeName.GetHashCode() : 0) * 397) ^ (MethodName != null ? MethodName.GetHashCode() : 0);
            }
        }
    }
}
