﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using NUnit.Framework;
using PowerAssert;
using PowerAssert.Infrastructure;
using PowerAssert.Infrastructure.Nodes;

namespace PowerAssertTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestFixture]
    public class EndToEndTest
    {


        [Test]
        public void PrintResults()
        {
            int x = 11;
            int y = 6;
            DateTime d = new DateTime(2010, 3, 1);
            Expression<Func<bool>> expression = () => x + 5 == d.Month * y;
            Node constantNode = ExpressionParser.Parse(expression.Body);
            string[] strings = NodeFormatter.Format(constantNode);
            string s = string.Join(Environment.NewLine, strings);
            Console.Out.WriteLine(s);
        }

        [Test]
        
        public void RunComplexExpression()
        {
            int x = 11;
            int y = 6;
            DateTime d = new DateTime(2010, 3, 1);
            PAssert.That(() => x + 5 == d.Month * y);
        }

        static int field = 11;
        [Test]
        
        public void RunComplexExpressionWithStaticField()
        {
            int y = 6;
            DateTime d = new DateTime(2010, 3, 1);
            PAssert.That(() => field + 5 == d.Month * y);
        }

        [Test]
        
        public void RunComplexExpression2()
        {
            string x = " lalalaa ";
            int i = 10;
            PAssert.That(() => x.Trim().Length == Math.Max(4, new int[] { 5, 4, i / 3, 2 }[0]));
        }

        [Test]
        
        public void RunComplexExpression3()
        {
            List<int> l = new List<int> { 1, 2, 3 };
            bool b = false;
            PAssert.That(() => l[2].ToString() == (b ? "three" : "four"));
        } 
        
        [Test]
        
        public void RunStringCompare()
        {
            string s = "hello, bobby";
            Tuple<string> t = new Tuple<string>("hello, Bobby");
            PAssert.That(() => s == t.Item1);
        }

        [Test]
        
        public void RunRoundingEdgeCase()
        {
            double d = 3;
            int i = 2;


            PAssert.That(() => 4.5 == d + 3 / i);
        }

        [Test]
        
        public void EqualsButNotOperatorEquals()
        {
            var t1 = new Tuple<string>("foo");
            var t2 = new Tuple<string>("foo");

            PAssert.That(() => t1 == t2);
        }

        [Test]
        
        public void SequenceEqualButNotOperatorEquals()
        {
            object list = new List<int>{1,2,3};
            object array = new[] { 1, 2, 3 };
            PAssert.That(() => list == array);
        }

        [Test]
        
        public void PrintingLinqStatements()
        {
            var list = Enumerable.Range(0, 150);
            PAssert.That(() => list.Where(x=>x%2==0).Sum() == 0);
        }

        [Test]
        
        public void PrintingLinqExpressionStatements()
        {
            var list = Enumerable.Range(0, 150);
            PAssert.That(() => (from l in list where l%2==0 select l).Sum() == 0);
        }

        [Test]
        
        public void PrintingComplexLinqExpressionStatements()
        {
            var list = Enumerable.Range(0, 5);
            PAssert.That(() => !(from x in list from y in list where x > y select x + "," + y).Any());
        }

        [Test]
        
        public void PrintingEnumerablesWithNulls()
        {
            var list = new List<int?>{1,2,null,4,5};
            PAssert.That(() => list.Sum() == null);
        }
        
        [Test]
        
        public void PrintingUnaryNot()
        {
            var b = true;
            PAssert.That(() => !b);
        } 
        
        [Test]
        
        public void PrintingUnaryNegate()
        {
            var b = 5;
            PAssert.That(() => -b==0);
        }

        [Test]
        
        public void PrintingIsTest()
        {
            var b = new object();
            PAssert.That(() => b is string);
        }
    }
}
