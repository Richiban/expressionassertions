﻿using NUnit.Framework;
using PowerAssert;
using System.Linq;

namespace PowerAssertTests
{
    [TestFixture]
    class RunThroughTests
    {
        [Test]
        public void RunThrough()
        {
            var c = 3;
            var d = 4;

            PAssert.That(() => c == d);
        }

        [Test]
        public void RunThrough2()
        {
            var a = "one";
            var b = "t";

            //StringAssert.Contains(a, b);
            PAssert.That(() => a.Contains(b));
        }
    }
}
